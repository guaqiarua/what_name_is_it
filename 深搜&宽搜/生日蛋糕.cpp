#include<bits/stdc++.h>
using namespace std;
const int Max=1000000000;
int m,n,ans=Max;
void dfs(int no,int a,int h,int rest,int s){
	if(a<0||h<0||rest<0)return;
	if(a*a*h*no<rest)return;
	if(s>ans)return;
	if(!no){
		if(!rest)ans=min(ans,s);
		return;
	}
	for(int i=a;i>=no;--i)
		for(int j=h;j>=no;--j){
			if(no==n)
				dfs(no-1,i-1,j-1,rest-i*i*j,s+2*i*j+i*i);
			else
				dfs(no-1,i-1,j-1,rest-i*i*j,s+2*i*j);
		}
}
int main(){
	cin>>m>>n;
	double k=sqrt(m);
	dfs(n,k,k,m,0);
	cout<<ans%Max<<endl;
	return 0;
}
