#include <iostream>
using namespace std;
struct node{
	int a[4];
	int step,fa;
}q[1000005];
int v[5],f,r;
bool used[104][104][104];
int sx,sy,sz;
void gqr(){
	while(f<r){
		int xx=q[++f].a[1],yy=q[f].a[2],zz=q[f].a[3];
		for(int i=1;i<=3;++i){
			for(int j=1;j<=3;++j){
				if(i==j)continue;
				int b[4];
				b[1]=xx;b[2]=yy;b[3]=zz;
				b[j]+=b[i];b[i]=0;
				if(b[j]>v[j]){
					b[i]=b[j]-v[j];
					b[j]=v[j];
				}
				if(!used[b[1]][b[2]][b[3]]){
					++r;
					used[b[1]][b[2]][b[3]]=1;
					for(int k=1;k<=3;++k)q[r].a[k]=b[k];
					q[r].fa=f;q[r].step=q[f].step+1; 
					if(b[1]==sx&&b[2]==sy)return;
				}
			}
		} 
	}
}
void prn(int k){
	if(k==-1)return;
	prn(q[k].fa);
	cout<<q[k].a[1]<<' '<<q[k].a[2]<<' '<<q[k].a[3]<<endl;
}
int main(){
	for(int i=1;i<=3;++i)cin>>v[i];
	cin>>q[++r].a[1]>>q[r].a[2]>>q[r].a[3];
	cin>>sx>>sy>>sz;
	q[r].fa=-1;q[r].step=1;
	used[q[r].a[1]][q[r].a[2]][q[r].a[3]]=1;
	gqr();
	cout<<q[r].step<<endl;
	prn(r);
	return 0;
}
