#include<bits/stdc++.h>
using namespace std;
const int N=1000005;
inline int read(){
	int x=0;bool f=0;char ch;
	while(!isdigit(ch=getchar()))f|=ch=='-';
	while(isdigit(ch))x=(x<<1)+(x<<3)+(ch^48),ch=getchar();
	return f?-x:x;
}
inline void write(int x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
int n,m,ans;
int a[N];
int lt[N],st[N],ed[N];
struct node{
	int min,add;
}tree[N<<2];
inline void Build(int k,int l,int r){
	if(l==r){
		tree[k].min=a[l];
		return;
	}
	int mid=(l+r)>>1;
	Build(k<<1,l,mid);Build(k<<1|1,mid+1,r);
	tree[k].min=min(tree[k<<1].min,tree[k<<1|1].min);
}
inline void spread(int k){
	if(tree[k].add){
		tree[k<<1].min+=tree[k].add;
		tree[k<<1|1].min+=tree[k].add;
		tree[k<<1].add+=tree[k].add;
		tree[k<<1|1].add+=tree[k].add;
		tree[k].add=0;
	}
}
inline void add(int k,int l,int r,int x,int y,int v){
	if(x<=l&&y>=r){
		tree[k].add+=v;
		tree[k].min+=v;
		return;
	}
	spread(k);
	int mid=(l+r)>>1;
	if(x<=mid)add(k<<1,l,mid,x,y,v);
	if(y>mid)add(k<<1|1,mid+1,r,x,y,v);
	tree[k].min=min(tree[k<<1].min,tree[k<<1|1].min);
}
inline int getmin(int k,int l,int r,int x,int y){
	if(x<=l&&y>=r)return tree[k].min;
	spread(k);
	int ret=999999999;
	int mid=(l+r)>>1;
	if(x<=mid)ret=min(ret,getmin(k<<1,l,mid,x,y));
	if(y>mid)ret=min(ret,getmin(k<<1|1,mid+1,r,x,y));
	return ret;
}
inline void work(){
	for(int i=1;i<=m;++i)
		if(getmin(1,1,n,st[i],ed[i])>=lt[i])
			add(1,1,n,st[i],ed[i],-lt[i]);
		else{
			ans=i;return;
		}
}
int main(){
	n=read(),m=read();
	for(int i=1;i<=n;++i)a[i]=read();
	for(int i=1;i<=m;++i)lt[i]=read(),st[i]=read(),ed[i]=read();
	Build(1,1,n);work();
	if(ans)printf("-1\n");
	write(ans);
	return 0;
}
