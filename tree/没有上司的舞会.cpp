#include<bits/stdc++.h>
using namespace std;
const int maxn=23333;
int n,cnt,t;
int dp[maxn][2];
int head[maxn];
int fa[maxn];
bool vis[maxn][2];
struct node{
	int u,nxt;
}e[maxn];
void addegde(int u,int v){
	e[++cnt]=(node){v,head[u]};
	head[u]=cnt;
}
int dfs(int u,int f){
	if(vis[u][f])return dp[u][f];
	vis[u][f]=1;
	if(!head[u])return dp[u][f]=f*dp[u][1];
	if(f)
		for(int v=head[u];v;v=e[v].nxt)
			dp[u][f]+=dfs(e[v].u,0);
	else
		for(int v=head[u];v;v=e[v].nxt)
			dp[u][f]+=max(dfs(e[v].u,1),dfs(e[v].u,0));
	return dp[u][f];
}
int main(){
	cin>>n;
	for(int i=1;i<=n;++i)
		cin>>dp[i][1];
	for(int i=1;i<=n;++i){
		int u,v;cin>>v>>u;
		if(!v&&!u)continue;
		addegde(u,v);fa[v]=u;
	}
	for(int i=1;i<=n;++i)
		if(!fa[i])t=i;
	cout<<max(dfs(t,0),dfs(t,1))<<endl;
	return 0;
}