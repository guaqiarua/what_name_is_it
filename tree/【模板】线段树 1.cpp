#include<bits/stdc++.h>
#define ll long long
#define lson k<<1
#define rson k<<1|1
using namespace std;
const int N=100005;
int n,m,cnt;
struct node{
	int l,r;
	ll sum,add;
	#define l(k) tree[k].l
	#define r(k) tree[k].r
	#define sum(k) tree[k].sum
	#define add(k) tree[k].add
}tree[N<<2];
inline void build(int k,int x,int y){
	l(k)=x;r(k)=y;
	if(x==y){
		scanf("%lld",&sum(k));
		return;
	}
	int mid=(x+y)>>1;
	build(lson,x,mid);build(rson,mid+1,y);
	sum(k)=sum(lson)+sum(rson);
}
inline void clear(int k){
	if(add(k)){
		add(lson)+=add(k);
		add(rson)+=add(k);
		sum(lson)+=(r(lson)-l(lson)+1)*add(k);
		sum(rson)+=(r(rson)-l(rson)+1)*add(k);
		add(k)=0;
	}
}
inline void Add(int k,int x,int y,ll z){
	if(x<=l(k)&&y>=r(k)){
		sum(k)+=(r(k)-l(k)+1)*z;
		add(k)+=z;
		return;
	}
	clear(k);
	int mid=(l(k)+r(k))>>1;
	if(x<=mid)Add(lson,x,y,z);
	if(y>mid)Add(rson,x,y,z);
	sum(k)=sum(lson)+sum(rson);
}
inline ll Ask(int k,int x,int y){
	if(x<=l(k)&&y>=r(k))return sum(k);
	clear(k);
	ll ret=0;
	int mid=(l(k)+r(k))>>1;
	if(x<=mid)ret+=Ask(lson,x,y);
	if(y>mid)ret+=Ask(rson,x,y);
	return ret;
}
int main(){
	scanf("%d%d",&n,&m);
	build(1,1,n);
	for(int i=1,pos,x,y,z;i<=m;++i){
		scanf("%d%d%d",&pos,&x,&y);
		if(pos==1){
			scanf("%d",&z);
			Add(1,x,y,z);
		}else printf("%lld\n",Ask(1,x,y));
	}
	return 0;
}