#include<bits/stdc++.h>
#define qia(i,a,b) for(int i=(a);i<=(b);++i)
#define rua(i,a,b) for(int i=(a);i>=(b);--i)
#define ll long long
#define It set<int>::iterator
using namespace std;
const int maxn=100005;
inline ll read(){
    register ll x=0,f=0,ch;
    while(!isdigit(ch=getchar()))f|=ch=='-';
    while(isdigit(ch))x=(x<<1)+(x<<3)+(ch^48),ch=getchar();
    return f?-x:x;
}
inline void write(ll x) {
    if(x<0)putchar('-'),x=-x;
    if(x>9)write(x/10);
    putchar(x%10+'0');
}
set<int>s;
struct edge{int no,nxt,l;}gua[maxn<<1];
ll m,n,ans;
ll cnt,cnt1;
int head[maxn];
ll lg[maxn],dep[maxn],len[maxn];
ll in[maxn],dfn[maxn];
ll up[maxn][23],dis[maxn][23];
void addedge(int l,int v,int u){
    gua[++cnt]=(edge){v,head[u],l},head[u]=cnt;
    gua[++cnt]=(edge){u,head[v],l},head[v]=cnt;
}
void dfs(int u,int f){
    dep[u]=dep[f]+1,up[u][0]=f;
    for(int i=1;i<=lg[dep[u]];++i)
      up[u][i]=up[up[u][i-1]][i-1];
    for(int i=head[u];i;i=gua[i].nxt){
    	int v=gua[i].no;
    	if(v==f)continue;
    	len[v]=len[u]+gua[i].l;
    	dfs(v,u);
    }
}
int Lca(int a,int b){
    if(dep[a]<dep[b])swap(a,b);
    while(dep[a]>dep[b])a=up[a][lg[dep[a]-dep[b]]];
    if(a==b)return a;
    for(int i=lg[dep[a]];i>=0;--i) 
        if(up[a][i]!=up[b][i])a=up[a][i],b=up[b][i];
    return up[a][0];
}
ll dist(int a,int b){
	int lca=Lca(a,b);
	return len[a]+len[b]-2*len[lca];
}
void redfs(int u){
	dfn[u]=++cnt1;in[cnt1]=u;
	for(int i=head[u];i;i=gua[i].nxt){
		int v=gua[i].no;
		if(!dfn[v])redfs(v);
	}
}
It l(It it){
	if(it==s.begin())return--s.end();
    return --it;
}
It r(It it){
	if(it==--s.end())return s.begin();
    return ++it;
}
int main(){
    n=read();
    qia(i,2,n)lg[i]=lg[i/2]+1;
    qia(i,1,n-1)addedge(read(),read(),read());
	dfs(1,0);redfs(1);
	m=read();
	while(m--){
		int ch=getchar();
		while(ch!='+'&&ch!='-'&&ch!='?')ch=getchar();
		It it;int t;
		if(ch=='?')write(ans/2),putchar('\n');
		else{
			int x=read();
			if(ch=='+'){
				if(s.size()){
					it=s.lower_bound(dfn[x]);
					if(it==s.end())it=s.begin();
					t=*l(it);
					ans+=dist(x,in[t])+dist(x,in[*it])-dist(in[t],in[*it]);
				}
				s.insert(dfn[x]);
			}
			else{
				it=s.find(dfn[x]);
                t=*l(it),it=r(it);
                ans-=dist(x,in[t])+dist(x,in[*it])-dist(in[t],in[*it]);
                s.erase(dfn[x]);
			}
		}
	}
    return 0;
}