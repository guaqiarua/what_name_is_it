#include<iostream>
#include<cstring>
#include<stdio.h>
#define qia(i,a,b) for(int i=(a);i<=(b);++i)
using namespace std;
const int maxn=10005;
inline int read(){
    register int x=0,f=0,ch;
    while(!isdigit(ch=getchar()))f|=ch=='-';
    while(isdigit(ch))x=(x<<1)+(x<<3)+(ch^48),ch=getchar();
    return f?-x:x;
}
inline void write(int x) {
    if(x<0)putchar('-'),x=-x;
    if(x>9)write(x/10);
    putchar(x%10+'0');
}
struct edge{int no,nxt,l;}gua[maxn<<1];
int cnt;
int head[maxn];
int dp[maxn];
int dep[maxn];
int lg[maxn];
int up[maxn][23];
int len[maxn];
void addedge(int l,int v,int u){
    gua[++cnt]=(edge){v,head[u],l},head[u]=cnt;
    gua[++cnt]=(edge){u,head[v],l},head[v]=cnt;
}
void dfs(int u,int f){
    dep[u]=dep[f]+1,up[u][0]=f;
    for(int i=1;i<=lg[dep[u]];++i)
      up[u][i]=up[up[u][i-1]][i-1];
    for(int i=head[u];i;i=gua[i].nxt){
    	int v=gua[i].no;
    	if(v==f)continue;
    	len[v]=len[u]+gua[i].l;
    	dfs(v,u);
    }
}
int Lca(int a,int b){
    if(dep[a]<dep[b])swap(a,b);
    while(dep[a]>dep[b])a=up[a][lg[dep[a]-dep[b]]-1];
    if(a==b)return a;
    for(int i=lg[dep[a]]-1;i>=0;--i) 
        if(up[a][i]!=up[b][i])a=up[a][i],b=up[b][i];
    return up[a][0];
}
int m,n;
int main(){
    n=read(),m=read();
    qia(i,1,n)lg[i]=lg[i-1]+((1<<lg[i-1])==i);
    qia(i,1,n-1)addedge(read(),read(),read());
    dfs(1,0);
    qia(i,1,m){
        int u=read(),v=read();
    	int lca=Lca(u,v);
    	write(len[u]+len[v]-2*len[lca]);
    	putchar('\n');
    }
    return 0;
}
