#include<iostream>
#include<cstring>
#include<stdio.h>
using namespace std;
const int maxn=500005;
int fa[maxn];
int dep[maxn];
int n,m,f;
int lg[maxn];
inline int read() {
    register int x=0,f=0,ch;
    while(!isdigit(ch=getchar()))f|=ch=='-';
    while(isdigit(ch))x=(x<<1)+(x<<3)+(ch^48),ch=getchar();
    return f?-x:x;
}
inline void write(int x) {
    if(x<0) putchar('-'),x=-x;
    if(x>9) write(x/10);
    putchar(x%10+'0');
}
struct edge{
    int no,nxt;
}gua[2*maxn];
int head[maxn];
int cnt;
void addedge(int u,int v){
    gua[++cnt]=(edge){v,head[u]};head[u]=cnt;
    gua[++cnt]=(edge){u,head[v]};head[v]=cnt;
}
bool vis[maxn];
int up[maxn][233];
void dfs(int u,int f){
    dep[u]=dep[f]+1,fa[u]=f;
    up[u][0]=f;
    for(int i=1;i<=lg[dep[u]];++i)
      up[u][i]=up[up[u][i-1]][i-1];
    for(int i=head[u];i;i=gua[i].nxt)
      if(gua[i].no!=f)dfs(gua[i].no,u);
}
int lca(int a,int b){
    if(dep[a]<dep[b])swap(a,b);
    while(dep[a]>dep[b])a=up[a][lg[dep[a]-dep[b]]];
    if(a==b)return a;
    for(int i=lg[dep[a]];i>=0;--i) 
        if(up[a][i]!=up[b][i])a=up[a][i],b=up[b][i];
    return up[a][0];
}
int main(){
    cin>>n>>m>>f;
    for(int i=2;i<=n;++i)
      lg[i]=lg[i/2]+1;
    for(int i=1;i<n;++i)
        addedge(read(),read());
    dfs(f,0);
    for(int i=1;i<=m;++i){
        write(lca(read(),read()));
        putchar('\n');
    }
    return 0;
}
