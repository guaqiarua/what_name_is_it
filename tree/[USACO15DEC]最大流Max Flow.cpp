#include<iostream>
#include<cstring>
#include<stdio.h>
using namespace std;
const int maxn=50005;
inline int read(){
    register int x=0,f=0,ch;
    while(!isdigit(ch=getchar()))f|=ch=='-';
    while(isdigit(ch))x=(x<<1)+(x<<3)+(ch^48),ch=getchar();
    return f?-x:x;
}
inline void write(int x) {
    if(x<0)putchar('-'),x=-x;
    if(x>9)write(x/10);
    putchar(x%10+'0');
}
struct edge{int no,nxt;}gua[maxn*2];
int cnt;
int head[maxn];
int dp[maxn];
int fa[maxn];
int dep[maxn];
int lg[maxn];
int up[maxn][23];
void addedge(int u,int v){
    gua[++cnt]=(edge){v,head[u]},head[u]=cnt;
    gua[++cnt]=(edge){u,head[v]},head[v]=cnt;
}
void dfs(int u,int f){
    dep[u]=dep[f]+1,fa[u]=up[u][0]=f;
    for(int i=1;i<=lg[dep[u]];++i)
      up[u][i]=up[up[u][i-1]][i-1];
    for(int i=head[u];i;i=gua[i].nxt)
      if(gua[i].no!=f)dfs(gua[i].no,u);
}
int Lca(int a,int b){
    if(dep[a]<dep[b])swap(a,b);
    while(dep[a]>dep[b])a=up[a][lg[dep[a]-dep[b]]-1];
    if(a==b)return a;
    for(int i=lg[dep[a]]-1;i>=0;--i) 
        if(up[a][i]!=up[b][i])a=up[a][i],b=up[b][i];
    return up[a][0];
}
void gg(int u,int fa){
	for(int i=head[u];i;i=gua[i].nxt){
		int v=gua[i].no;
		if(v==fa)continue;
		gg(v,u);
		dp[u]+=dp[v];
	}
}
int m,n,ans;
int main(){
	n=read(),m=read();
	for(int i=1;i<=n;++i)
		lg[i]=lg[i-1]+((1<<lg[i-1])==i);
	for(int i=1;i<n;++i)
		addedge(read(),read());
	dfs(1,0);
	for(int i=1;i<=m;++i){
		int u=read(),v=read();
		int lca=Lca(u,v);
		++dp[u],++dp[v],--dp[lca],--dp[fa[lca]];
	}
	gg(1,0);
	for(int i=1;i<=n;++i)
		ans=max(ans,dp[i]);
	write(ans);putchar('\n');
	return 0;
}
