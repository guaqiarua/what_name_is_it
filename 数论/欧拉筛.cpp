#include<bits/stdc++.h>
using namespace std;
inline int read(){
	int f=0,ch,x=0;
	while(!isdigit(ch=getchar()))f|=ch=='-';
	while(isdigit(ch))x=(x<<1)+(x<<3)+(ch^48),ch=getchar();
	return f?-x:x;
}
int m,n,k;
int prim[10000005];
bool f[10000005];
int main(){
	n=read(),m=read();f[1]=1;
	for(int i=2;i<=n;++i){
		if(!f[i])prim[++k]=i;
		for(int j=1;j<=k&&i*prim[j]<=n;j++){
        	f[i*prim[j]]=1;
        	if(!(i%prim[j]))break;
    	}
	}
	for(int i=1;i<=m;++i)
		if(!f[read()])cout<<"Yes\n";
		else cout<<"No\n";
	return 0;
}
