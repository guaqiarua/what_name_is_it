#include<bits/stdc++.h>
using namespace std;
const int maxn=3005;
int n,d;
int k1[maxn],k2[maxn];
int dp[maxn];
int main(){
    cin>>n>>d;
    for(int i=1;i<=n;++i){
        char x;cin>>x;
        k1[i]=k1[i-1]+(x=='H');
        k2[i]=k2[i-1]+(x=='J');
    }
    memset(dp,0x3f,sizeof dp);
    dp[0]=0;
    for(int i=1;i<=n;++i){
        for(int j=i;j>=1;--j){
            int n1=k1[i]-k1[j-1];
            int n2=k2[i]-k2[j-1];
            if(!n1||!n2||abs(n1-n2)<=d)
                dp[i]=min(dp[i],dp[j-1]+1);
        }
    }
    cout<<dp[n]<<endl;
    return 0;
}
