#include <iostream>
#include <vector>
using namespace std;
vector<int>s[100000+5];
int n;
bool vis[100000+5];
void dfs(int u){
	vis[u]=1;
	cout<<u<<' ';
	for(unsigned i=0;i<s[u].size();++i){
		int v=s[u][i];
		if(!vis[v])dfs(v);
	}
//	vis[u]=0;
}
int main(){
	cin>>n;
	for(int i=1;i<n;++i){
		int x,y;
		cin>>x>>y;
		s[x].push_back(y);
		s[y].push_back(x);
	}
	for(int i=1;i<=n;++i)
		if(!vis[i])dfs(i);
	return 0;
}
