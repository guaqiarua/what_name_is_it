#include <bits/stdc++.h>
using namespace std;
#define For(i,a,b) for(int i=(a);i<=(b);++i)
struct point{
	int x,y;
};
set<point>P;//允许有重复元素 multiset<point>P;
bool operator < (point a,point b){
	return a.x<b.x||(a.x==b.x&&a.y<b.y);
}//重载运算符 < 
bool operator == (point a,point b){
	return a.x==b.x&&a.y==b.y;
}//重载运算符 ==
int n;
int main(){
	cin>>n;
	For(i,1,n){
		int p,q;cin>>p>>q;
		point tmp;tmp.x=p;tmp.y=q;//point tmp=(point){p,q};
		P.insert(tmp);
	}
	for(set<point>::iterator it=P.begin();it!=P.end();++it)
		cout<<'('<<it->x<<','<<it->y<<')'<<'\n';//遍历 
	return 0;
}
