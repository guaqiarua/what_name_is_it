#include <bits/stdc++.h>
using namespace std;
int n;
const int inf=999999999;
int a[205];
int sum[205];
int f1[205][205];
int f2[205][205];
int main(){
	cin>>n;
	for(int i=1;i<=n;++i){
		cin>>a[i];
		a[i+n]=a[i];
	}
	for(int i=1;i<=2*n;++i)
		sum[i]=sum[i-1]+a[i];
	for(int p=1;p<n;p++)
        for(int i=1;i<2*n-p;i++){  
        	int j=i+p;
            f1[i][j]=inf;
            for(int k=i;k<j;k++){  
                f1[i][j] = min(f1[i][j], f1[i][k]+f1[k+1][j]);   
                f2[i][j] = max(f2[i][j], f2[i][k]+f2[k+1][j]);  
            } 
            f1[i][j]+=sum[j]-sum[i-1];
            f2[i][j]+=sum[j]-sum[i-1];
        }  
	int ans1=inf,ans2=0;
	for(int i=1;i<=n;++i){
		ans1=min(f1[i][i+n-1],ans1);
		ans2=max(f2[i][i+n-1],ans2);
	}
	cout<<ans1<<endl<<ans2<<endl;
	return 0;
}
