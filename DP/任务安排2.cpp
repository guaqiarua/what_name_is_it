#include<bits/stdc++.h>
#define ll long long
using namespace std;
const int N=300005;
inline int read(){
	int x=0;bool f=0;char ch;
	while(!isdigit(ch=getchar()))f|=ch=='-';
	while(isdigit(ch))x=(x<<1)+(x<<3)+(ch^48),ch=getchar();
	return f?-x:x;
}
const ll Inf=0x3f3f3f3f;
int n,s,f,r;
ll dp[N];
ll sumT[N],sumC[N];
int q[N];
int main(){
	n=read();s=read();
	for(int i=1;i<=n;++i){
		sumT[i]=read();sumC[i]=read();
		sumT[i]+=sumT[i-1];
		sumC[i]+=sumC[i-1];
	}
	for(int i=1;i<=n;++i)dp[i]=Inf;
	for(int i=1;i<=n;++i){
		while(f<r&&dp[q[f+1]]-dp[q[f]]<=(s+sumT[i])*(sumC[q[f+1]]-sumC[q[f]]))++f;
		dp[i]=dp[q[f]]-(sumT[i]+s)*sumC[q[f]]+sumT[i]*sumC[i]+s*sumC[n];
		while(f<r&&(dp[q[r]]-dp[q[r-1]])*(sumC[i]-sumC[q[r]])>=(dp[i]-dp[q[r]])*(sumC[q[r]]-sumC[q[r-1]]))--r;
		q[++r]=i;
	}
	cout<<dp[n]<<endl;
	return 0;
}