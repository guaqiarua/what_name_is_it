#include<bits/stdc++.h>
#define gua pair<int,int>
#define ll long long
using namespace std;
struct num{ll no[233];};
ll rua[110];
map<gua,num>dp;
int n;
void check(ll k[]){
    for(int i=1;i<=k[0];++i){
        k[i+1]+=k[i]/10000;
        k[i]%=10000;
    }
    while(k[k[0]+1]){
        ++k[0];
        k[k[0]+1]+=k[k[0]]/10000;
        k[k[0]]%=10000;
    }
}
void mul(ll k[],ll a,ll b,ll c){
    k[0]=k[1]=1;
    for(int i=1;i<=k[0];++i)k[i]*=a;
    check(k);
    for(int i=1;i<=k[0];++i)k[i]*=b;
    check(k);
    for(int i=1;i<=k[0];++i)k[i]*=c;
    check(k);
}
void add(ll a[],ll b[],ll c[]){
    c[0]=max(a[0],b[0]);
    for(int i=1;i<=c[0];++i)
    	c[i]=a[i]+b[i];
    check(c);
}
void prn(int k){
    cout<<k/1000;
    cout<<k%1000/100;
    cout<<k%100/10;
    cout<<k%10;
}
void write(ll a[]){
    cout<<a[a[0]];
    for(int i=a[0]-1;i>=1;--i)
        prn(a[i]);
    cout<<endl;
}
bool Min(ll a[],ll b[]){
    if(a[0]!=b[0])return a[0]<b[0];
    for(int i=a[0];i>=1;--i)
        if(a[i]!=b[i])return a[i]<b[i];
    return 0;
}
void Copy(ll a[],ll b[]){
    for(int i=0;i<=a[0];++i)a[i]=0;
    for(int i=0;i<=b[0];++i)a[i]=b[i];
}
int main(){
    cin>>n;
    for(int i=1;i<=n;++i){
    	cin>>rua[i];
    	rua[i+n]=rua[i];
	}
    for(int i=1;i<2*n;++i){
        gua tmp=make_pair(i,i+1);
        dp[tmp].no[0]=1;
    }
    for(int len=2;len<n;++len)
        for(int i=1;i<=2*n-len;++i){
            int j=i+len;
            gua tmp,tmp1,tmp2;
            ll p1[233],p2[233],p3[233];
            tmp=make_pair(i,j);
            dp[tmp].no[0]=60;
            for(int k=i+1;k<j;++k){
                memset(p1,0,sizeof p1);
                memset(p2,0,sizeof p2);
                memset(p3,0,sizeof p3);
                p1[0]=p2[0]=p3[0]=1;
                tmp1=make_pair(i,k);
                tmp2=make_pair(k,j);
                mul(p1,rua[i],rua[k],rua[j]);
                add(dp[tmp1].no,dp[tmp2].no,p2);
                add(p1,p2,p3);
                if(Min(p3,dp[tmp].no))Copy(dp[tmp].no,p3);
            }
        }
    ll ans[110];
    Copy(ans,dp[make_pair(1,n)].no);
    for(int i=2;i<=n;++i)
    	if(Min(dp[make_pair(i,i+n-1)].no,ans))
    		Copy(ans,dp[make_pair(i,i+n-1)].no);
    write(ans);
    return 0;
}
