#include<bits/stdc++.h>
using namespace std;
int m,n,l;
int f[2333][66];
int check(int k){
	int a[11];
	for(int i=0;i<m;++i)
		a[i]=k&(1<<i);
	int t=0;
	for(int i=0;i<m;++i){
		if(a[i])++t;
		else{
			if(t%2)return 0;
			else t=0;
		}
	}
	if(t%2)return 0;
	return 1;
}
int main(){
	scanf("%d%d",&n,&m);
	l=1<<m;
	for(int i=0;i<l;++i)f[1][i]=check(i);
	for(int i=2;i<=n;++i)
		for(int j=0;j<l;++j)
			for(int k=0;k<l;++k)
				if((j|k)==l-1&&f[1][j&k])
					f[i][j]=(f[i-1][k]+f[i][j])%1000000007;
	printf("%d\n",f[n][l-1]);
	return 0;
}
