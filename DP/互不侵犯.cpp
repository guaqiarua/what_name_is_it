#include<bits/stdc++.h>
using namespace std;
int n,m,cnt,pos;
long long ans;
int rua[105];
int num[105];
long long f[12][105][105];
inline bool gg(int k){
	return !(k&(k<<1));
}
inline int Count(int k){
	int ret=0;
	while(k){
		++ret;
		k-=k&(-k);
	}
	return ret;
}
int main(){
	scanf("%d%d",&n,&m);
	pos=(1<<n)-1;
	for(int i=0;i<pos;++i)
		if(gg(i))rua[++cnt]=i;
	for(int i=1;i<=cnt;++i)
		num[i]=Count(rua[i]);
	for(int i=1;i<=cnt;++i)
		f[1][i][num[i]]=1;
	for(int i=2;i<=n;++i)
		for(int j=1;j<=cnt;++j)
			for(int k=1;k<=cnt;++k){
				int a=rua[j],b=rua[k];
				if((a&b)||(a&(b<<1))||((a<<1)&b))continue;
				for(int l=0;l<=m;++l)
					f[i][j][num[j]+l]+=f[i-1][k][l];
			}
	for(int i=1;i<=cnt;++i)ans+=f[n][i][m];
	printf("%lld",ans);
	return 0;
}
