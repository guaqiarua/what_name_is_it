#include<bits/stdc++.h>
using namespace std;
const int N=5005;
inline int read(){
	int x=0;bool f=0;char ch;
	while(!isdigit(ch=getchar()))f|=ch=='-';
	while(isdigit(ch))x=(x<<1)+(x<<3)+(ch^48),ch=getchar();
	return f?-x:x;
}
const int Inf=0x3f3f3f3f;
int n,s;
int dp[N];
int sumT[N],sumC[N];
int main(){
	n=read();s=read();
	for(int i=1;i<=n;++i){
		sumT[i]=read();sumC[i]=read();
		sumT[i]+=sumT[i-1];
		sumC[i]+=sumC[i-1];
	}
	for(int i=1;i<=n;++i)dp[i]=Inf;
	for(int i=1;i<=n;++i)
		for(int j=0;j<i;++j)
			dp[i]=min(dp[i],dp[j]+sumT[i]*(sumC[i]-sumC[j])+s*(sumC[n]-sumC[j]));
	cout<<dp[n]<<endl;
	return 0;
}
