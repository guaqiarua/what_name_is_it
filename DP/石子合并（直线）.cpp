#include <bits/stdc++.h>
using namespace std;
int n;
int sum[405];
int f[405][405];
int fa[405][405];
int main(){
	cin>>n;
	for(int i=1;i<=n;++i){
		int t;cin>>t;
		sum[i]=sum[i-1]+t;
	}
	memset(f,0x3f,sizeof(f));
	for(int i=1;i<=n;++i)f[i][i]=0;
	for(int i=n;i>=1;--i)
		for(int j=i+1;j<=n;++j)
			for(int k=i;k<j;++k)
				f[i][j]=min(f[i][j],f[i][k]+f[k+1][j]+sum[j]-sum[i-1]);
	cout<<f[1][n]<<endl;
	return 0;
}
