#include<bits/stdc++.h>
using namespace std;
const int N=21;
int n;
int mp[N][N];
int dp[1<<N][N];//在访问s集合中的点且最后一个点为k时的最短路径 
inline int dfs(int s,int v){//集合s表示哪些点去过，v表示当前节点 
	if(dp[s][v]!=-1)return dp[s][v];
	if(s==(1<<n)-1&&v==n-1)return dp[s][v]=0;//所有点已访问，并且最后一个点为n-1 
	int Min=999999999;
	for(int i=0;i<n;++i){
		if(s&(1<<i))continue;//该点已访问 
		int k=dfs(s|(1<<i),i)+mp[v][i];//打擂开始 
		if(k<Min)Min=k;
	}
	return dp[s][v]=Min;
}
int main(){
	scanf("%d",&n);
	for(int i=0;i<n;++i)
		for(int j=0;j<n;++j)
			scanf("%d",&mp[i][j]);
	memset(dp,-1,sizeof dp);
	printf("%d",dfs(1,0)); //初始集合 从0出发  PS题目要求从0出发到达n-1点，所以初始集合开始为1 
	return 0;
}
