#include<bits/stdc++.h>
#define gua(i,a,b) for(int i=(a);i<=(b);++i)
using namespace std;
const int maxn=10005;
const int maxm=100005;
set<pair<int,int> >Eg;
stack<int>rua;
int m,n,cnt,num,gqr,ct,ans;
int a[maxn],newa[maxn];
int head[maxn],newhead[maxn];
int len[maxn],newlen[maxn];
bool vis[maxn],newvis[maxn];
int dfn[maxn],low[maxn];
int belong[maxn];
struct edge{
	int v,nxt;
}e[maxm<<1],newe[maxn<<1];
void addedge(int u,int v){
	e[++cnt]=(edge){v,head[u]},head[u]=cnt;
}
void tarjan(int u){
	dfn[u]=low[u]=++num;
	rua.push(u);vis[u]=1;
	for(int i=head[u];i;i=e[i].nxt){
		int v=e[i].v;
		if(!dfn[v]){
			tarjan(v);
			low[u]=min(low[u],low[v]);
		}
		else if(vis[v])
			low[u]=min(low[u],dfn[v]);
	}
	if(dfn[u]==low[u]){
		newa[++gqr]=u;
		int v=0;
		while(v!=u){
			v=rua.top();
			rua.pop();
			len[u]+=a[v];
			belong[v]=u;
			vis[v]=0;
		}
	}
}
void add(int x){
	int u=belong[x];
	for(int i=head[x];i;i=e[i].nxt){
		int v=e[i].v;pair<int,int> tmp=make_pair(u,belong[v]);
		if(belong[v]!=u&&Eg.find(tmp)==Eg.end()){
			newe[++ct]=(edge){belong[v],newhead[u]};
			newhead[u]=ct;Eg.insert(tmp);
		}
	}
}
void dfs(int u){
	newvis[u]=1;
	for(int i=newhead[u];i;i=newe[i].nxt){
		int v=newe[i].v;
		if(!newvis[v])dfs(v);
		newlen[u]=max(newlen[u],newlen[v]);
	}
	newlen[u]+=len[u];
	ans=max(ans,newlen[u]);
}
int main(){
	scanf("%d%d",&n,&m);
	gua(i,1,n)scanf("%d",&a[i]);
	gua(i,1,m){
		int u,v;
		scanf("%d%d",&u,&v);
		addedge(u,v);
	}
	gua(i,1,n)if(!dfn[i])tarjan(i);
	gua(i,1,n)add(i);
	gua(i,1,gqr)if(!newvis[newa[i]])dfs(newa[i]);
	printf("%d",ans);
	return 0;
}
