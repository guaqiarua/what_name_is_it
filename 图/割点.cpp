#include<bits/stdc++.h>
#define gua(i,a,b) for(int i=(a);i<=(b);++i)
using namespace std;
const int maxn=200005;
const int maxm=1000005;
inline int read(){
	int f=0,ch,x=0;
	while(!isdigit(ch=getchar()))f|=ch=='-';
	while(isdigit(ch))x=(x<<1)+(x<<3)+(ch^48),ch=getchar();
	return f?-x:x;
}
int m,n,cnt,rot,ctt,rot_son;
struct edge{
	int v,nxt;
}e[maxm<<1];
int head[maxn];
int low[maxn];
int dfn[maxn];
bool gg[maxn];
int ans[maxn];
void addedge(int u,int v){
	e[++cnt]=(edge){v,head[u]},head[u]=cnt;
	e[++cnt]=(edge){u,head[v]},head[v]=cnt;
}
void tarjan(int u){
	dfn[u]=low[u]=++cnt;
	for(int i=head[u];i;i=e[i].nxt){
		int v=e[i].v;
		if(!dfn[v]){
			tarjan(v);
			low[u]=min(low[u],low[v]);
			if(u==rot)++rot_son;
			else if(dfn[u]<=low[v])
				if(!gg[u])ans[++ctt]=u,gg[u]=true;
		}
		else low[u]=min(low[u],dfn[v]);
	}
}
int main(){
	n=read(),m=read();
	gua(i,1,m)addedge(read(),read());
	gua(i,1,n){
		if(!dfn[i]){
			rot=i,cnt=0,rot_son=0;
			tarjan(i);
			if(rot_son>1)ans[++ctt]=i;
		}
	}
	sort(ans+1,ans+ctt+1);
	cout<<ctt<<endl;
	gua(i,1,ctt){
		cout<<ans[i];
		i==ctt?cout<<'\n':cout<<' ';
	}
		
	return 0;
}
