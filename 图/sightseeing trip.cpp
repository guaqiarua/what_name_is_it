#include<bits/stdc++.h>
#define gua(i,a,b) for(int i=(a);i<=(b);++i)
using namespace std;
const int maxn=105;
const int INF=1e7; 
inline void write(int x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
int m,n,cnt;
int dis[maxn][maxn];
int nxt[maxn][maxn];
int w[maxn][maxn];
int path[maxn];
void init(){
	for(int i=1;i<=n;++i)
		for(int j=1;j<=n;++j)
			dis[i][j]=w[i][j]=INF;
}
int Floyed(){
   int ans=INF;
   for(int k=1;k<=n;k++){
      for(int i=1;i<k;i++)
         for(int j=i+1;j<k;j++){
            int tmp=dis[i][j]+w[i][k]+w[k][j];
            if(tmp<ans){
               cnt=0;ans=tmp;
               for(int p=i;p!=j;p=nxt[p][j])
                  path[++cnt]=p;
               path[++cnt]=j;path[++cnt]=k;
         	}
      }
      for(int i=1;i<=n;i++)
         for(int j=1;j<=n;j++)
            if(dis[i][k]+dis[k][j]<dis[i][j]){
               dis[i][j]=dis[i][k]+dis[k][j];
               nxt[i][j]=nxt[i][k];
            }
   }
   return ans;
}
int main(){
	cin>>n>>m;init();
	gua(i,1,m){
		int u,v,l;cin>>u>>v>>l;
		dis[u][v]=dis[v][u]=w[u][v]=w[v][u]=min(l,dis[u][v]);
		nxt[u][v]=v,nxt[v][u]=u;
	}
	if(Floyed()==INF)cout<<"No solution.\n";
	else{
		for(int i=1;i<=cnt;++i)
			cout<<path[i]<<' ';
		cout<<endl;
	}
	return 0;
}
