#include<bits/stdc++.h>
#define gua(i,a,b) for(int i=(a);i<=(b);++i)
#define Pair pair<int,int>
#define Make(a,b) make_pair((a),(b))
using namespace std;
const int maxn=1000005;
const int maxm=2000005;
inline int read(){
	int f=0,ch,x=0;
	while(!isdigit(ch=getchar()))f|=ch=='-';
	while(isdigit(ch))x=(x<<1)+(x<<3)+(ch^48),ch=getchar();
	return f?-x:x;
}
inline void write(int x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
priority_queue<Pair>rua;
int head[maxn];
int cnt,m,n;
int d[maxn];
int ct[maxn];
bool vis[maxn];
struct edge{
	int v,nxt,l;
}e[maxm<<1];
void addedge(int v,int u){
	e[++cnt]=(edge){v,head[u]},head[u]=cnt;
	e[++cnt]=(edge){u,head[v]},head[v]=cnt;
}
void dijkstra(int u){
	memset(d,0x3f,sizeof d);
	memset(vis,0,sizeof vis);
	memset(ct,0,sizeof ct);ct[1]=1;
	rua.push(Make(d[u]=0,u));
	int k=0;
	while(rua.size()&&k<n){
		int x=rua.top().second;rua.pop();
		if(vis[x])continue;
		vis[x]=1;++k;
		for(int i=head[x];i;i=e[i].nxt){
			int y=e[i].v;
			if(d[y]>d[x]+1){
				d[y]=d[x]+1;ct[y]=ct[x];
				rua.push(Make(-d[y],y));
			}
			else if(d[y]==d[x]+1){
				ct[y]+=ct[x];
				ct[y]%=100003;
			}
		}
	}
}
int main(){
	n=read(),m=read();
	gua(i,1,m)addedge(read(),read());
	dijkstra(1);
	gua(i,1,n)write(ct[i]),putchar('\n');
	return 0;
}
