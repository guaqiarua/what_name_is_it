#include<bits/stdc++.h>
#define gua(i,a,b) for(int i=(a);i<=(b);++i)
#define gqr(i,u) for(int i=head[u];i;i=nxt(i))
using namespace std;
const int maxn=50005;
inline int read(){
	int x=0,f=0;char ch;
	while(!isdigit(ch=getchar()))f|=ch=='-';
	while(isdigit(ch))x=(x<<1)+(x<<3)+(ch^48),ch=getchar();
	return f?-x:x;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
struct edge{
	int v,nxt,l;
	#define l(i) e[i].l
	#define v(i) e[i].v
	#define nxt(i) e[i].nxt
}e[maxn<<1];
int cnt,T,m,n;
int head[maxn],ans[maxn],vis[maxn],d[maxn],fa[maxn];
vector<int> rua[maxn],ruaid[maxn];
void addedge(int u,int v,int l){
	e[++cnt]=(edge){v,head[u],l},head[u]=cnt;
	e[++cnt]=(edge){u,head[v],l},head[v]=cnt;
}
void add(int x,int y,int k){
	rua[x].push_back(y),ruaid[x].push_back(k);
	rua[y].push_back(x),ruaid[y].push_back(k);
}
int get(int x){
	if(x==fa[x])return x;
	return fa[x]=get(fa[x]);
}
void tarjan(int u){
	vis[u]=1;
	gqr(i,u){
		int v=v(i);
		if(vis[v])continue;
		d[v]=d[u]+l(i);
		tarjan(v);
		fa[v]=u;
	}
	for(unsigned i=0;i<rua[u].size();++i){
		int v=rua[u][i],id=ruaid[u][i];
		if(vis[v]==2){
			int lca=get(v);ans[id]=lca;
			ans[id]=min(ans[id],d[u]+d[v]-2*d[lca]);
		}
	}
	vis[u]=2;
}
int main(){
	T=read();
	while(T--){
		n=read(),m=read();
		gua(i,1,n){
			fa[i]=i;head[i]=0;vis[i]=0;
			rua[i].clear();ruaid[i].clear();
		}
		cnt=0;
		gua(i,1,m){
			int u=read(),v=read(),l=read();
			addedge(u,v,l);
			ans[i]= 1 << 30;
			if(u==v)ans[i]=0;
			else add(u,v,i);
		}
		tarjan(1);
		gua(i,1,m)write(ans[i]),putchar('\n');
	}
	return 0;
}
