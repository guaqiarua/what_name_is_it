#include<bits/stdc++.h>
#define gua(i,a,b) for(int i=(a);i<=(b);++i)
using namespace std;
const int maxn=100005;
const int maxm=200005;
inline int read(){
	int f=0,ch,x=0;
	while(!isdigit(ch=getchar()))f|=ch=='-';
	while(isdigit(ch))x=(x<<1)+(x<<3)+(ch^48),ch=getchar();
	return f?-x:x;
}
inline void write(int x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
int cnt,m,n,s;
int head[maxn]; 
int d[maxn];
bool vis[maxn];
struct edge{
	int v,nxt,l;
}e[maxm<<1];
void addedge(int l,int v,int u){
	e[++cnt]=(edge){v,head[u],l},head[u]=cnt;
}
priority_queue<int,vector<int>,greater<int> >q;
void spfa(int s){
	memset(d,0x3f3f3f3f,sizeof d);
	memset(vis,0,sizeof vis);
	d[s]=0;vis[s]=1;q.push(s);
	while(q.size()){
		int x=q.top();q.pop();vis[x]=0;
		for(int i=head[x];i;i=e[i].nxt){
			int y=e[i].v,z=e[i].l;
			if(d[y]>d[x]+z){
				d[y]=d[x]+z;
				if(!vis[y])q.push(y),vis[y]=1;
			}
		}
	}
}
int main(){
	n=read(),m=read(),s=read();
	gua(i,1,m)addedge(read(),read(),read());
	spfa(s);
	gua(i,1,n-1)write(d[i]),putchar(' ');
	write(d[n]),putchar('\n');
	return 0;
}
