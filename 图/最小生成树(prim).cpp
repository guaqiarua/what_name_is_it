#include<bits/stdc++.h>
#define gua(i,a,b) for(int i=(a);i<=(b);++i)
#define Pair pair<int,int>
#define Make(a,b) make_pair((a),(b))
using namespace std;
const int maxn=100005;
const int maxm=200005;
inline int read(){
	int f=0,ch,x=0;
	while(!isdigit(ch=getchar()))f|=ch=='-';
	while(isdigit(ch))x=(x<<1)+(x<<3)+(ch^48),ch=getchar();
	return f?-x:x;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
priority_queue<Pair>rua;
int head[maxn];
int cnt,m,n,s;
int d[maxn];
bool vis[maxn];
struct edge{
	int v,nxt,l;
}e[maxm<<1];
void addedge(int l,int v,int u){
	e[++cnt]=(edge){v,head[u],l},head[u]=cnt;
	e[++cnt]=(edge){u,head[v],l},head[v]=cnt;
}
int prim(){
	int k=0,ans=0;
	memset(d,0x3f,sizeof d);
	memset(vis,0,sizeof vis);
	d[1]=0;rua.push(Make(0,1));
	while(rua.size()&&k<n){
		int x=rua.top().second;rua.pop();
		if(vis[x])continue;
		vis[x]=1;++k;ans+=d[x];
		for(int i=head[x];i;i=e[i].nxt){
			int y=e[i].v,z=e[i].l;
			if(!vis[y]&&d[y]>z){
				d[y]=z;
				rua.push(Make(-d[y],y));
			}
		}
	}
	return k==n?ans:-1;
}
int main(){
	n=read(),m=read();
	gua(i,1,m)addedge(read(),read(),read());
	write(prim()),putchar(10);
	return 0;
}
