#include<iostream>
#include<cstring>
#include<cstdio>
#define gua(i,a,b) for(int i=(a);i<=(b);++i)
#define N 0x3f3f3f3f
using namespace std;
const int maxn=10005;
int read(){
	int f=0,x=0;char ch;
	while(!isdigit(ch=getchar()))f|=ch=='-';
	while(isdigit(ch))x=(x<<1)+(x<<3)+(ch^48),ch=getchar();
	return f?-x:x;
}
int n,k1,k2;
int d[maxn/10];
int ld[maxn],rd[maxn],dd[maxn];
int lb[maxn],rb[maxn],bb[maxn];
void work(){
	gua(i,2,n)d[i]=N;d[1]=0;
	gua(k,1,n){
		gua(i,1,n-1)if(d[i+1]<N)d[i]=min(d[i],d[i+1]);
		gua(i,1,k1)if(d[lb[i]]<N)d[rb[i]]=min(d[rb[i]],d[lb[i]]+bb[i]);
		gua(i,1,k2)if(d[ld[i]]<N)d[ld[i]]=min(d[ld[i]],d[rd[i]]-dd[i]);
	}
}
int main(){
	n=read(),k1=read(),k2=read();
	gua(i,1,k1)lb[i]=read(),rb[i]=read(),bb[i]=read();
	gua(i,1,k2)ld[i]=read(),rd[i]=read(),dd[i]=read();
	work();
	if(d[n]==N)cout<<"-2\n";
	else if(d[1]<0)cout<<"-1\n";
	else cout<<d[n]<<endl;
	return 0;
}
