#include<bits/stdc++.h>
#define gua(i,a,b) for(int i=(a);i<=(b);++i)
using namespace std;
const int maxn=10005;
const int maxm=50005;
inline int read(){
    int f=0,ch,x=0;
    while(!isdigit(ch=getchar()))f|=ch=='-';
    while(isdigit(ch))x=(x<<1)+(x<<3)+(ch^48),ch=getchar();
    return f?-x:x;
}
int m,n;
int head[maxn],cnt;
struct edge{
    int v,nxt;
}e[maxm<<1];
void addedge(int v,int u){
    e[++cnt]=(edge){v,head[u]},head[u]=cnt;
}
stack<int>rua;
int num,ct;
int dfn[maxn],low[maxn];
int belong[maxn];
int du[maxn],siz[maxn];
void tarjan(int u){
    dfn[u]=low[u]=++num;
    rua.push(u);
    for(int i=head[u];i;i=e[i].nxt){
        int v=e[i].v;
        if(!dfn[v]){
            tarjan(v);
            low[u]=min(low[u],low[v]);
        }
        else if(!belong[v])
            low[u]=min(low[u],dfn[v]);
    }
    if(dfn[u]==low[u]){
    	++ct;
    	int v;
    	do{
    		v=rua.top();
    		rua.pop();
    		belong[v]=ct;
    		++siz[ct];
		}while(v!=u);
    }
}
int main(){
    n=read(),m=read();
    gua(i,1,m)addedge(read(),read());
    gua(i,1,n)if(!dfn[i])tarjan(i);
    gua(u,1,n)
    	for(int i=head[u];i;i=e[i].nxt){
    		int v=e[i].v;
     	    if(belong[u]!=belong[v])
             	du[belong[u]]++;
        }
    int ans,l=0;
    gua(i,1,ct)
        if(!du[i])ans=siz[i],++l;
    if(l==1)cout<<ans<<endl;
    else cout<<0<<endl;
    return 0;
}