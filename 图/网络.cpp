#include<bits/stdc++.h>
#define gua(i,a,b) for(int i=(a);i<=(b);++i)
using namespace std;
const int maxn=100005;
int n;
bool gg[maxn];
int cnt,num,ctt;
int rot,rot_son;
int head[maxn];
int low[maxn];
int dfn[maxn];
struct edge{
    int v,nxt;
}e[maxn<<1];
void addedge(int u,int v){
    e[++cnt]=(edge){v,head[u]};head[u]=cnt;
    e[++cnt]=(edge){u,head[v]};head[v]=cnt;
}
void tarjan(int u){
    dfn[u]=low[u]=++cnt;
    for(int i=head[u];i;i=e[i].nxt){
        int v=e[i].v;
        if(!dfn[v]){
            tarjan(v);
            low[u]=min(low[u],low[v]);
            if(u==rot)++rot_son;
            else if(dfn[u]<=low[v])
                if(!gg[u])++ctt,gg[u]=true;
        }
        else low[u]=min(low[u],dfn[v]);
    }
}
int main(){
    while(cin>>n){
        if(!n)break;
        memset(head,0,sizeof head);
        memset(low,0,sizeof low);
        memset(dfn,0,sizeof dfn);
        memset(gg,0,sizeof gg);
        cnt=num=ctt=0;
        int u;
        while(cin>>u){
            if(!u)break;
            char x;
            while((x=getchar())!='\n'){
                int v;cin>>v;
                addedge(u,v);
            }
        }
        gua(i,1,n){
            if(!dfn[i]){
                rot=i,rot_son=0;
                tarjan(i);
                if(rot_son>1)++ctt;
            }
        }
        cout<<ctt<<endl;
    }
    return 0;
}