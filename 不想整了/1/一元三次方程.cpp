#include <bits/stdc++.h>
using namespace std;
double a,b,c,d;
double ans[5];
int t;
double gg(double k){
	return a*k*k*k+b*k*k+c*k+d;
}
void qr(double m,double n){
	while(n-m>0.00001){
		double mid=(m+n)/2;
		if(gg(mid)*gg(m)>0)m=mid;
		else n=mid;
	}
	ans[++t]=m;
}
int main(){
	cin>>a>>b>>c>>d;
	for(double i=-100;i<200;++i)
		if(gg(i)==0)ans[++t]=i;
		else if(gg(i)*gg(i+1)<0)qr(i,i+1);
	if(gg(200)==0)ans[++t]=200;
	for(int i=1;i<=t;++i)
		cout<<fixed<<setprecision(2)<<ans[i]<<' ';
	cout<<endl;
	return 0;
}
