#include <bits/stdc++.h>
using namespace std;
#define For(i,a,b) for(int i=(a);i<=(b);++i)
int m,n,zmax;
int a[101][101],l[101][101];
bool flag[101][101];
int footstep[101][101];
int dx[4]={0,0,1,-1},dy[4]={1,-1,0,0};
int f(int x,int y)
{
    if(flag[x][y])return l[x][y];
    flag[x][y]=1;footstep[x][y]=1;
    for(int i=0;i<4;++i){
        int xx=x+dx[i],yy=y+dy[i];
        if(xx>0&&xx<=m&&yy>0&&yy<=m&&a[xx][yy]<a[x][y]&&!footstep[xx][yy])
            l[x][y]=max(l[x][y],f(xx,yy)+1);
    }
    footstep[x][y]=0;
    return l[x][y];
}
int main()
{
    cin>>m>>n;
    For(i,1,m)For(j,1,n)cin>>a[i][j];
    For(i,1,m)For(j,1,n)l[i][j]=1;
    For(i,1,m)For(j,1,n)zmax=max(f(i,j),zmax);
    cout<<zmax<<endl; 
    return 0;
}
