#include <iostream>
using namespace std;
const int N=1005;
int mp[N][N],ans[N][N];
int n;
int main(){
	cin>>n;
	for(int i=1;i<=n;++i)
		for(int j=1;j<=i;++j)
			cin>>mp[i][j];
	for(int i=n;i>=1;--i)
		for(int j=1;j<=i;++j)
			ans[i][j]=mp[i][j]+max(ans[i+1][j],ans[i+1][j+1]);
	cout<<ans[1][1]<<endl;
	return 0;
}
