#include <bits/stdc++.h>
using namespace std;
int f[150][105];
int len[150];
int n,k,ans=0x3f3f3f3f;
int main(){
	cin>>n>>k;
	for(int i=1;i<=n;++i)cin>>len[i];
	if(2*(k+3)>n)cout<<-1;
	else{
		sort(len+1,len+n+1);
		memset(f,0x3f3f3f3f,sizeof(f));
		f[0][0]=0;
		for(int i=1;i<=k+3;++i)
			for(int j=2;j<=n;++j)
				f[i][j]=min(f[i][j-1],f[i-1][j-2]+(len[j]-len[j-1])*(len[j]-len[j-1]));
		for(int i=1;i<=n;++i)ans=min(ans,f[k+3][i]);
		cout<<ans<<endl;	
	}
	return 0;
}
